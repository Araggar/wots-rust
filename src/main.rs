extern crate cryptoxide;
extern crate rand;
extern crate rand_chacha;

use std::str;
use rand::{Rng};//, SeedableRng};//, StdRng};
use crate::rand_chacha::rand_core::SeedableRng;


use self::cryptoxide::digest::Digest;
use self::cryptoxide::sha2::Sha256;

// use ring::digest::Digest
// use ring::digest::{Context, Digest, SHA256};
// https://rust-lang-nursery.github.io/rust-cookbook/cryptography/hashing.html


fn main() {
    println!("Hello, world!");
}


// TESTS

#[cfg(test)]
mod tests {
    use super::*;

    // CONSTS
    const SEED: u64 = 1337;
    const DATA: &str = "I have grown to love secrecy. \
                        It seems to be the one thing that can make modern life mysterious or marvelous to us. \
                        The commonest thing is delightful if only one hides it.";

    #[test]
    fn sign_ok() {
        let seed = SEED;
        let data = DATA.to_string();
        let sign = WotsSign::sign(&data, &seed, 16, 256).unwrap();

        assert!(sign.verify(&data, 16));
    }
}


// STRUCTS

struct WotsSign {
    sign: Vec<String>,
    p_key: Vec<String>,
}

#[derive(Debug)]
struct KeyPair { // Wots Key Pair
    w: usize,
    n: usize,
    s_key: Vec<String>,
    p_key: Vec<String>,
    t: usize,
    t1: usize,
    t2: usize,
}

// STRUCT IMPL

impl WotsSign {
    fn sign(data: &String, seed: &u64, w: usize, n: usize) -> Result<WotsSign, &'static str> {
        let key_pair = KeyPair::new(&seed, w, n).unwrap();
        let mut sign: Vec<String> = vec![]; // Can be static
        //let mut sign: Vec<[u8;32]> = Vec::with_capacity(key_pair.t); 

        let data_digest = digest_str(data); 
        let data_w = hex_to_16(&data_digest);

        let mut out = &mut [0;32];
        for (i, chunk) in data_w.iter().enumerate() {
            sign.push(chain_digest(&key_pair.s_key[i], &(*chunk as usize)));
        }

        println!("\n----- SIGN OK -----\n");
        Ok( WotsSign{ sign, p_key: key_pair.p_key} )
    }

    fn verify(&self, data: &String, w: usize) -> bool {
        let data_digest = digest_str(data); 
        let data_w = hex_to_16(&data_digest);

        let mut aux: Vec<String> = vec![]; // Can be static

        let mut out = &mut [0;32];
        
        for (i,chunk) in data_w.iter().enumerate() {
            aux.push(chain_digest(&self.sign[i], &(w-(*chunk as usize))));
        }

        let mut match_ = true;
        for (xi, yi) in self.p_key.iter().zip(aux.iter_mut()) {
            match_ &= xi == yi;
        }
        match_
    }
}

impl KeyPair {
    fn new(seed: &u64, w: usize, n: usize) -> Result<KeyPair, &'static str> {
        let (s_key, p_key) = KeyPair::gen_keys(&seed, &w, &n);
        Ok( KeyPair{ w, n, s_key, p_key, t:KeyPair::t(&w,&n), t1:KeyPair::t1(&w,&n), t2:KeyPair::t2(&w,&n) } )
    }

    fn t(w: &usize, n: &usize) -> usize {
        KeyPair::t1(w,n) + KeyPair::t2(w,n)
    }

    fn t1(w: &usize, n: &usize) -> usize {
        ((*n as f64)/(*w as f64).log2()).ceil() as usize
    }

    fn t2(w: &usize, n: &usize) -> usize {
        (((KeyPair::t1(&w,&n)*(*w-1)) as f64).log2()/(*w as f64).log2()).floor() as usize
    }

    fn gen_keys(seed: &u64, w: &usize, n: &usize) -> (Vec<String>, Vec<String>) {
        let skey = KeyPair::gen_skey(&seed, &w, &n);
        let pkey = KeyPair::gen_pkey(&skey, &w, &n);
        (skey, pkey)
    }

    fn gen_skey(seed: &u64, w: &usize, n: &usize) -> Vec<String> {  // TODO Byte vector
        let mut rng = rand_chacha::ChaCha8Rng::seed_from_u64(*seed);
        
        let size = KeyPair::t(w,n);

        let mut out: Vec<String> = Vec::with_capacity(size);

        for _ in 0..size {
            //out.push(rng.gen::<usize>());
            out.push(format!("{:X}",rng.gen::<usize>()));
        }
        out
    }

    fn gen_pkey(skey: &Vec<String>, w: &usize, n: &usize) -> Vec<String> { // TODO digest the key to save space, Byte vector
        let mut out: Vec<String> = Vec::with_capacity(KeyPair::t(w,n));
        
        for sk in skey {
            out.push(chain_digest(&sk, w));        
        }
        out
    }

}

// METHODS

fn base_w(data: &mut [u8], logw: usize) -> Vec<u8> {  // TODO verify correctness
    let bytes = data;
    let out_len = (bytes.len()*8)/logw;
    let mut out: Vec<u8> = Vec::with_capacity(out_len);

    let mut inc = 0;
    let mut total = 0;
    let mut bits = 0;

    for _ in 0..out_len {
        if bits == 0 {
            total = bytes[inc];
            inc += 1;
            bits += 8;
        }
        bits -= logw;
        out.push( total >> bits & ( (1 << logw) - 1 ));
    }
    out
}

fn base_w_str(data: &String, logw: usize) -> Vec<u8> {  // TODO verify correctness
    let bytes = data.as_bytes();
    let out_len = (bytes.len()*8)/logw;
    let mut out: Vec<u8> = Vec::with_capacity(out_len);

    let mut inc = 0;
    let mut total = 0;
    let mut bits = 0;

    for _ in 0..out_len {
        if bits == 0 {
            total = bytes[inc];
            inc += 1;
            bits += 8;
        }
        bits -= logw;
        out.push( total >> bits & ( (1 << logw) - 1 ));
    }
    out
}

fn digest_str(data: &String) -> String {
    let mut hasher = Sha256::new();
    hasher.input_str(data);
    hasher.result_str()
}

fn digest(data: &[u8], out: &mut [u8]) {
    let mut hasher = Sha256::new();
    hasher.input(data);
    hasher.result(out);
}

fn chain_digest_bits(data: &String, it: &usize, out: &mut [u8; 32]) {
    let mut bytes = data.as_bytes();

    digest(bytes, out);
    for _ in 1..*it {
        digest(bytes, out);
    }
}


fn chain_digest(data: &String, it: &usize) -> String {
    let mut hex = data.clone();  // TODO: Remove Clone

    for _ in 0..*it {
        hex = digest_str(&hex);
    }
    hex
}

fn hex_to_16(data: &String) -> Vec<u8> {
    let mut out = Vec::with_capacity(data.len());
    for token in data.chars() {
        out.push(token.to_digit(16).unwrap() as u8);
    }

    out
}

